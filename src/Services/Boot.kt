package com.example.Services

import com.example.Services.Es.ElasticSearch
import kotlin.system.exitProcess

/**
 * This class check connection from external service/databases
 */
open class Boot {

    fun bootProject() {
        if (!checkES()) {
            println("ES Connection Failed !")
            exitProcess(1)
        }
    }

    private fun checkES(): Boolean {
        return try {
            println("---Connexion to ES---")
            ElasticSearch.searchRequest()
            println("---Connected to ES---")
            true
        } catch (e: Exception) {
            println("Unknow ES error" + e.printStackTrace())
            false
        }
    }
}