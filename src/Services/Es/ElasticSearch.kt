package com.example.Services.Es

import com.example.Model.Product
import com.example.Model.Search
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.typesafe.config.ConfigFactory
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.lucene.util.QueryBuilder
import org.elasticsearch.action.get.GetRequest
import org.elasticsearch.action.get.GetResponse
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.*
import org.elasticsearch.common.unit.Fuzziness
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import java.util.*

object ElasticSearch : RestHighLevelClient(client) {

    private fun getTestIndex(): String? {
        return ConfigFactory.load().getString("elasticsearch.indexes.test")
    }

    private fun getProductIndex(): String? {
        return ConfigFactory.load().getString("elasticsearch.indexes.product")
    }

    //To test if everything ok when the app boot
    fun searchRequest() {
        val testIndex = getTestIndex()

        val searchRequest = SearchRequest(testIndex)
        searchRequest.types("doc")

        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.query(QueryBuilders.matchAllQuery())
        searchRequest.source(searchSourceBuilder)
        ElasticSearch.search(searchRequest)
    }

    //To find all product find with the keyword
    fun searchProductRequest(search: Search): SearchResponse? {

        val productIndex = getProductIndex()

        //Index to search
        val searchRequest = SearchRequest(productIndex)
        searchRequest.types("doc")

        //prepare Query with search word and fuzziness
        val matchQueryBuilders = QueryBuilders.matchQuery("shortLabel", search.name)
            .fuzziness(Fuzziness.AUTO)

        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.query(matchQueryBuilders)

        searchRequest.source(searchSourceBuilder)

        //Get all documents from search
        return ElasticSearch.search(searchRequest)

    }

    //Insert product in Elastic Search product index
    fun insertProduct(product: Product) {
        val jsonProduct = jacksonObjectMapper().writeValueAsString(product)
        val productIndex = getProductIndex()

        val request = IndexRequest(
            productIndex,
            "doc",
            UUID.randomUUID().toString()
        )
        request.source(jsonProduct, XContentType.JSON)
        val indexResponse = ElasticSearch.index(request)
    }
}

//connect and make client for ES
val client: RestClientBuilder?
    get() {
        try {

            val login = ConfigFactory.load().getString("elasticsearch.login")
            val password = ConfigFactory.load().getString("elasticsearch.password")
            val host = ConfigFactory.load().getString("elasticsearch.host")
            val scheme = ConfigFactory.load().getString("elasticsearch.scheme")
            val port = ConfigFactory.load().getInt("elasticsearch.port")

            val credentialsProvider = BasicCredentialsProvider()
            credentialsProvider.setCredentials(AuthScope.ANY, UsernamePasswordCredentials(login, password))

            return RestClient.builder(HttpHost(host, port, scheme)).setHttpClientConfigCallback { httpClientBuilder ->
                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }


//val builder: RestClientBuilder?
//    get() {
//        try {
//
//            val login = ConfigFactory.load().getString("elasticsearch.login")
//            val password = ConfigFactory.load().getString("elasticsearch.password")
//            val host = ConfigFactory.load().getString("elasticsearch.host")
//            val scheme = ConfigFactory.load().getString("elasticsearch.scheme")
//            val port = ConfigFactory.load().getInt("elasticsearch.port")
//
//            val credentialsProvider = BasicCredentialsProvider()
//            credentialsProvider.setCredentials(AuthScope.ANY, UsernamePasswordCredentials(login, password))
//
//            return RestClient.builder(HttpHost(host, port, scheme)).setHttpClientConfigCallback { httpClientBuilder ->
//                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return null
//    }
