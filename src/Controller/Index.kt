package com.example.Controller

import com.example.Model.*
import com.example.Services.Es.ElasticSearch
import com.example.SimpleJWT
import io.ktor.application.call
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.ContentType
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

/**
 * All routes of the API
 */
fun Route.index(simpleJWT: SimpleJWT) {

    get("/ping") {
        call.respondText("pong", contentType = ContentType.Text.Plain)
    }

    //JWT auth to put product from php-ag-miniproject
    route("/product"){
        authenticate {
            post {
                val post = call.receive<Product>()
                val principal = call.principal<UserIdPrincipal>() ?: error("No user principal connected")
                ElasticSearch.insertProduct(post)
                call.respond(mapOf("OK" to true))
            }
        }
    }

    //Route to search in Elastic Search index of products
    post("/search") {
        val post = call.receive<Search>()
        val products = ElasticSearch.searchProductRequest(post)
        call.respond("response" to products?.hits)
    }

    //Login&Register route to do before use POST "/product" route
    post("/login-register") {
        val post = call.receive<LoginRegister>()
        val user = users.getOrPut(post.user) { User(post.user, post.password) }
        if (user.password != post.password) throw InvalidCredentialsException("Invalid credentials")
        call.respond(mapOf("token" to simpleJWT.sign(user.name)))
    }
}