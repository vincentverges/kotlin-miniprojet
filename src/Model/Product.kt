package com.example.Model

data class Product(
    val id: Int?,
    val rawLabel: String?,
    val shortLabel: String?,
    val quantity: String?,
    val unitPrice: String?,
    val packageUnity: String?,
    val mass: String?,
    val flag: ArrayList<String>?,
    val price: String?,
    val rules: ArrayList<String>?,
    val productData: ProductData?
)