package com.example.Model

import java.util.*

class User(val name: String, val password: String)

val users = Collections.synchronizedMap(
    listOf(User("test","test"))
        .associateBy { it.name }
        .toMutableMap()
)