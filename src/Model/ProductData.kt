package com.example.Model

data class ProductData(
    val id: Int?,
    val name: String?,
    val averagePrice: String?,
    val category: String?,
    val brand: String?,
    val gtin: String?
)