package com.example.Model

class InvalidCredentialsException(message: String) : RuntimeException(message)