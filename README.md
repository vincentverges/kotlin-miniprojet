# kotlin-miniproject

## Install :

Clone the project from this [project](https://gitlab.com/Winciwizard/kotlin-miniproject) 

`https://gitlab.com/Winciwizard/kotlin-miniproject.git`

After enter to the source folder of the project :

* Make a `./gradllew`
* Update the resources/application.conf file with your local credentials with you PostgreSQL database
* Launch application in IntelliJ

## Routes :

#### POST "/search" :

Find specific product with keyword

The template of the search for the request body:

```$xslt
{
	"name": "salade"
}
```

#### POST "/product" :

Add a product to Elastic Search index

Need to have JWT Auth.

The template of the search for the request body:

```$xslt
{
	"rawLabel": "Salade Bio",
	"quantity": "1",
	"unitPrice": "1.99",
	"packageUnity": "g",
	"mass": "330",
	"flag": [],
	"price": "1.99",
	"productData": {
		"name": "Salade Verte",
		"averagePrice": "2.02",
		"category": "VEGETABLE",
		"brand": "Casino"
	}
}
```

## How it work

The POST "/product" route received a product JSON and use ElasticSearch service to post it in a specific index.

After products are indexed, we can use the POST "/search" route to make a research in all our product.